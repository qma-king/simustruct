package com.qmasoft.builder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

public class StructIO {
	private Idgeter idg;
	public String Author=null;
	public StructIO(Idgeter idg) {
		this.idg=idg;
	}
	public Vector<Map> Read(File file) throws IOException{
		Vector<Map> str=new Vector<Map>();
		File f=file;
		BufferedReader bf=new BufferedReader(new FileReader(f));
		String strsize[]=bf.readLine().split("x");
		int w=Integer.valueOf(strsize[0]),h=Integer.valueOf(strsize[1]),ls=Integer.valueOf(strsize[2]);
		str.setSize(ls);
		for(int l=0;l<ls;l++) {
			str.set(l, new Map(w,h));
			str.get(l).setHeight(h);
			str.get(l).setWidth(w);
		}
		String strref[]=bf.readLine().split(";");
		Properties prop=new Properties();
		//constant setting
		prop.setProperty("$",String.valueOf(idg.search("CONTR:0")));
		prop.setProperty("*",String.valueOf(idg.search("LIGHT:0")));
		//
		for(int i=0;i<strref.length;i++) {
			String eqex[]=strref[i].split("=");
			System.out.println(eqex[0]+"="+eqex[1]);
			if(eqex[0].equals("AU")) {
				Author=eqex[1];
				continue;
			}
			String s=String.valueOf(idg.search(eqex[1]));
			System.out.println(s);
			prop.setProperty(eqex[0],s);
//			for(int j=0;j<id.size();j++) {
//				if(eqex[1].equals(id.get(j))) {
//					prop.setProperty(eqex[0],String.valueOf(j));
//				}
//			}
		}
		int k=0;
		for(int l=0;l<ls;l++) {
			String line=bf.readLine();	
			for(int i=h-1;i>=0;i--) {
				for(int j=0;j<w;j++) {
//					System.out.println("k="+k+" i="+i+" j="+j);
					char c=line.charAt(k*w+j);
					String point=prop.getProperty(String.valueOf(c));
					if(point==null) {
						System.err.println("[Error]Block No "+c+" not found in structure data,replaced by 1");
						point="1";
					}
					str.get(l).setPoint(j, i,Integer.valueOf(point));
				}
				k++;
			}
			k=0;
		}
		bf.close();
		return str;
	}
	public void Write(File file,String author,int Width,int Height,int layerSum,Vector<Map> map,Idgeter idg) {
		File f=file;
		BufferedWriter bf;
		try {
			if(!f.exists())f.createNewFile();
			bf=new BufferedWriter(new FileWriter(f));
			bf.write(Width+"x"+Height+"x"+layerSum);
			bf.newLine();
			//analyze Type Sum
			Vector<Integer> an=new Vector<Integer>();
			an.setSize(idg.linesum);
			for(int i=0;i<an.size();i++)an.set(i, 0);
			for(int l=0;l<layerSum;l++) {
				Map m=map.get(l);
				for(int i=0;i<m.getHeight();i++) {
					for(int j=0;j<m.getWidth();j++) {
						int pi=m.getPoint(j, i);
						an.set(pi,an.get(pi)+1);
					}
				}
			}
			//analyze and write
			char c='A';
			for(int i=0;i<an.size();i++) {
				if(an.get(i)!=0) {
					String ids=idg.getIDString(i);
					if(ids.equals("LIGHT:0"))continue;
					else if(ids.equals("CONTR:0"))continue;
					bf.write(c+"="+ids+";");
					an.set(i,(int) c);
					c++;
				}
			}
			bf.write("AU="+author+";");
			bf.newLine();
			for(int l=0;l<layerSum;l++) {
				Map m=map.get(l);
				for(int i=m.getHeight()-1;i>=0;i--) {
					for(int j=0;j<m.getWidth();j++) {
						int bi=m.getPoint(j, i),it;
						char b;
						if(idg.getIDString(bi).equals("LIGHT:0")) {
							b='*';
						}else if(idg.getIDString(bi).equals("CONTR:0")) {
							b='$';
						}else {
							it=an.get(bi);
							b=(char)it;
						}
						bf.write(b);
					}
				}
				bf.newLine();
			}
			bf.flush();
			bf.close();
			System.out.println("finished");
			//
		} catch (IOException e1) {
			// TODO 自动生成的 catch 块
			e1.printStackTrace();
		}
	}
}
