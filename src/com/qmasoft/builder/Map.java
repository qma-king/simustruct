package com.qmasoft.builder;

import java.util.Vector;

public class Map {
	Vector<Vector<Integer>> map;
	private int width=32;
	private int height=32;
	
	public Map(int w,int h) {
		this.width=w;
		this.height=h;
		map=new Vector<Vector<Integer>>();
		map.setSize(height);
		for(int i=0;i<map.size();i++) {
			map.set(i,new Vector<Integer>());
			map.get(i).setSize(width);
			for(int j=0;j<width;j++) {
				map.get(i).set(j,0);
			}
		}
	}
	public Map() {
		this(32,32);
	}
	public void setWidth(int size) {
		for(int i=0;i<map.size();i++) {
			System.out.println(i);
			map.get(i).setSize(size);
		}
		if(size>this.width) {
			for(int l=0;l<map.size();l++) {
				for(int i=this.width-1;i<map.get(l).size();i++) {
					System.out.println(l+" "+i);
					map.get(l).set(i,0);
				}
			}
		}
		this.width=size;
	}
	public void setHeight(int size) {
		map.setSize(size);
		if(size>this.height) {
			for(int i=this.height-1;i<map.size();i++) {
				map.set(i,new Vector<Integer>());
				map.get(i).setSize(width);
				for(int j=0;j<width;j++) {
					map.get(i).set(j,0);
				}
			}
		}
		this.height=size;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public void setPoint(int x,int y,int val) {
		map.get(y).set(x,val);
	}
	public int getPoint(int x,int y) {
		return map.get(y).get(x);
	}
}
