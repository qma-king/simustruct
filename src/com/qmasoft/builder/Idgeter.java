package com.qmasoft.builder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JOptionPane;

public class Idgeter {
	private boolean successflag=true;
	public  int linesum=0;
	private Vector<String> id=new Vector<String>();
	private Vector<String> idname=new Vector<String>();
	public Idgeter() throws IOException {
		successflag=true;
		java.net.URL url = Idgeter.class.getProtectionDomain().getCodeSource()
                .getLocation();
        String filePath = null;
        try {
            filePath = java.net.URLDecoder.decode(url.getPath(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (filePath.endsWith(".jar"))
            filePath = filePath.substring(0, filePath.lastIndexOf("/") + 1);
		File f = new File(filePath+"\\List.txt");
        System.out.println("Absolute Path:"+f.getAbsolutePath());
		if(!f.exists()) {
			successflag=false;
			System.err.println("[Error]Cannot find "+f.getAbsolutePath());
	        JOptionPane.showMessageDialog(null, "无法找到List.txt，请确保程序与List.txt在同一目录下", "错误", JOptionPane.ERROR_MESSAGE, null);
			return;
		}
		BufferedReader fb=new BufferedReader(new FileReader(f));
		String item;
		while((item=fb.readLine())!=null) {
			linesum++;
			String temp[]=item.split(" ");
			id.add(temp[0]);
			idname.add(temp[1]+" "+temp[0]);
			System.out.println("[Reader Log]Readed ID:"+temp[0]+" Name="+temp[1]);
		}
		fb.close();
	}
	public boolean isSuccess() {
		return successflag;
	}
	public Vector<String> getList(){
		return idname;
	}
	public Vector<String> getId(){
		return id;
	}
	public String getIDString(int index) {
		return id.get(index);
	}
	public String getNameString(int index) {
		return idname.get(index);
	}
	public int search(String Id) {
		int out=1;
		boolean fflg=false;
		String ist[]=Id.split(":");
		for(int i=0;i<id.size();i++) {
			String st[]=id.get(i).split(":");
			if(st[0].equals(ist[0])) {
				if(!fflg) {
					out=i;
					fflg=true;
				}
				if(st[1].equals(ist[1])) {
					out=i;
					break;
				}
			}
		}
		return out;
	}
}
