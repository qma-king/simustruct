package com.qmasoft.builder;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JList;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.io.File;
//import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
//import java.nio.file.*;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.TransferHandler;

import java.awt.event.MouseAdapter;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.SpringLayout;
import java.awt.event.MouseMotionAdapter;
import javax.swing.JSpinner;
//import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.SpinnerNumberModel;

import java.awt.BasicStroke;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JSeparator;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import javax.swing.JToolBar;
import javax.swing.JToggleButton;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JSplitPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTabbedPane;

public class Main extends JFrame {

	/**
	 * SerialUID
	 */
	private static final long serialVersionUID = -6524700776248046569L;
	private JPanel contentPane,DrawingPane;
	private Idgeter idg=new Idgeter();
	private JLabel BlockStaus = new JLabel("\u6DFB\u52A0\u65B9\u5757\uFF1A\u672A\u9009\u4E2D\u65B9\u5757");
	private JTextField height_field,width_field,author;
	private JSpinner spinner= new JSpinner();
	private String selectedID=null;
	private File currentFile=new File("Untitled.txt");
	private Vector<Map> map;
	private Image img;
	private int Width=32,Height=32;
	private int currentLayer=1,layerSum=1;
	private StructIO sio = new StructIO(idg);
	private boolean blocktaking=false,grid=false;
	private JToggleButton taker;
	private JList<String> list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 */
	public Main() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException, IOException {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/com/qmasoft/images/blockConstruction.png")));
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		setTitle("Simukraft建筑编辑器--for1.7.10-[Untitled.txt]");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 631, 446);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("\u6587\u4EF6");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("\u65B0\u5EFA");
		mntmNewMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int a=JOptionPane.showConfirmDialog(mntmNewMenuItem, "你确定要新建吗？此操作无法撤销", getTitle(), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,null);
				if(a==JOptionPane.YES_OPTION) {
					currentLayer=1;
					author.setText("");
					layerSum=1;
					Width=32;
					Height=32;
					map.setSize(1);
					map.get(0).setHeight(32);
					map.get(0).setWidth(32);
					height_field.setText(String.valueOf(Height));
					width_field.setText(String.valueOf(Width));
//					System.out.println(Width+" "+Height);
					spinner.setValue(1);
					DrawingPane.setBounds(0,0,Width*16, Height*16);
					Dimension d=DrawingPane.getSize();
//					d.setSize(d.getWidth()+32,d.getHeight()+32);
					int w=(int) (d.getWidth()+32);
					int h=(int) (d.getHeight()+32);
					DrawingPane.setPreferredSize(new Dimension(w,h));
					height_field.setText(String.valueOf(Height));
					width_field.setText(String.valueOf(Width));
					clearCanvas(0);
				}
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem save_as = new JMenuItem("\u53E6\u5B58\u4E3A");
//		save_as.setMnemonic(KeyEvent.VK_S);
		save_as.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
		save_as.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save(true);
			}
		});
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("\u6253\u5F00");
		mntmNewMenuItem_5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser jf=new JFileChooser();
				int code=jf.showOpenDialog(contentPane);
				if(code==JFileChooser.CANCEL_OPTION)return;
				try {
					sio=new StructIO(idg);
					map=sio.Read(jf.getSelectedFile());
					setTitle("Simukraft建筑编辑器--for1.7.10-["+jf.getSelectedFile().getName()+"]");
					currentLayer=1;
					currentFile=jf.getSelectedFile();
					author.setText(sio.Author);
					layerSum=map.size();
					Width=map.get(0).getWidth();
					Height=map.get(0).getHeight();
//					ScrollSet(Width,Height);
//					System.out.println(Width+" "+Height);
					spinner.setValue(1);
					DrawingPane.setBounds(0,0,Width*16, Height*16);
					Dimension d=DrawingPane.getSize();
//					d.setSize(d.getWidth()+32,d.getHeight()+32);
					int w=(int) (d.getWidth()+32);
					int h=(int) (d.getHeight()+32);
					DrawingPane.setPreferredSize(new Dimension(w,h));
					height_field.setText(String.valueOf(Height));
					width_field.setText(String.valueOf(Width));
					DrawingPane.updateUI();
				} catch (IOException e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}
				fresh();
			}
		});
		mnNewMenu.add(mntmNewMenuItem_5);
		
		JMenuItem save = new JMenuItem("\u4FDD\u5B58");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save(!currentFile.exists());
			}
		});
		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		mnNewMenu.add(save);
		mnNewMenu.add(save_as);
		
		JSeparator separator = new JSeparator();
		mnNewMenu.add(separator);
		
		JMenu mnNewMenu_1 = new JMenu("\u6253\u5F00\u6700\u8FD1\u6587\u4EF6");
		mnNewMenu.add(mnNewMenu_1);
		
		JMenu mnNewMenu_2 = new JMenu("\u7F16\u8F91");
		menuBar.add(mnNewMenu_2);
		
		JMenu mnNewMenu_5 = new JMenu("\u6E05\u7A7A");
		mnNewMenu_2.add(mnNewMenu_5);
		
		JMenuItem clear_layer = new JMenuItem("\u6E05\u7A7A\u5F53\u524D\u5C42");
//		clear_layer.setMnemonic(KeyEvent.VK_E);
		clear_layer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.CTRL_MASK));
		clear_layer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int cho=JOptionPane.showConfirmDialog(null,"你确定要清除当前层级吗,操作无法撤销");
				if(cho==JOptionPane.NO_OPTION||cho==JOptionPane.CANCEL_OPTION)return;
				for(int i=0;i<Height;i++) {
					for(int j=0;j<Width;j++) {
						map.get(currentLayer-1).setPoint(j, i, 0);
					}
				}
				DrawingPane.updateUI();
				fresh();
			}
		});
		mnNewMenu_5.add(clear_layer);
		
		JMenu mnNewMenu_4 = new JMenu("\u8BBE\u7F6E");
		menuBar.add(mnNewMenu_4);
		
		JCheckBoxMenuItem gridBtn = new JCheckBoxMenuItem("\u4F7F\u7528\u7F51\u683C");
		gridBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				grid=gridBtn.isSelected();
				fresh();
			}
		});
		gridBtn.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_CLOSE_BRACKET, InputEvent.CTRL_DOWN_MASK));
		mnNewMenu_4.add(gridBtn);
		
		JMenu mnNewMenu_3 = new JMenu("\u5173\u4E8E");
		menuBar.add(mnNewMenu_3);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("\u5173\u4E8E\u4F5C\u8005");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "<html><h1>本软件由Q马制作</h1><br><h2>Copyright 2024-2025 Qmasoft,Shaksoft All Right Reserved<br></h2>", "关于", JOptionPane.INFORMATION_MESSAGE, null);
			}
		});
		mnNewMenu_3.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("\u8BBF\u95EEQ\u9A6C\u4E3B\u9875");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					openurl("https://qma-king.github.io/main");
				} catch (URISyntaxException | IOException e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}
			}
		});
		mnNewMenu_3.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("\u8BBF\u95EE\u4FA0\u5BA2\u76DF\u4E3B\u9875");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					openurl("https://shakmon.github.io");
				} catch (URISyntaxException | IOException e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}
			}
		});
		mnNewMenu_3.add(mntmNewMenuItem_3);
		
		JLabel lblNewLabel_5 = new JLabel("\u7248\u672C:3.0");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);
		mnNewMenu_3.add(lblNewLabel_5);
		
		JLabel lblNewLabel = new JLabel("\u5C42\u7EA7");
		menuBar.add(lblNewLabel);
		
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				System.out.println("[value]value="+spinner.getValue());
				if((int)spinner.getValue()>layerSum) {
					spinner.setValue(layerSum);
				}
//				DrawingPane.getGraphics().clearRect(0,0,DrawingPane.getWidth(),DrawingPane.getHeight());
				currentLayer=(int)spinner.getValue();
				DrawingPane.updateUI();
				fresh();
			}
		});
		spinner.setModel(new SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
		menuBar.add(spinner);
		
		JButton btnNewButton = new JButton("\u6DFB\u52A0\u5C42\u7EA7");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layerSum++;
				currentLayer++;
				map.add(new Map());
				DrawingPane.updateUI();
				spinner.setValue(currentLayer);
			}
		});
		menuBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u5220\u9664\u5C42\u7EA7");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(currentLayer==1)return;
				map.removeElementAt(currentLayer-1);
				if(currentLayer==layerSum) {
					currentLayer--;
				}
				layerSum--;
//				DrawingPane.updateUI();
				fresh();
				spinner.setValue(currentLayer);
			}
		});
		menuBar.add(btnNewButton_1);
		
		JLabel lblNewLabel_4 = new JLabel(" \u4F5C\u8005");
		menuBar.add(lblNewLabel_4);
		
		author = new JTextField();
		menuBar.add(author);
		author.setColumns(10);
		
		JButton btnNewButton_3 = new JButton("\u5237\u65B0\u7A97\u53E3");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fresh();
			}
		});
		menuBar.add(btnNewButton_3);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		
		contentPane.setTransferHandler(new TransferHandler() {
			/**
			 * UID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean importData(JComponent j,Transferable t) {
				try {
					Object o=t.getTransferData(DataFlavor.javaFileListFlavor);
					String filepath = o.toString();
                    if (filepath.startsWith("[")) {
                        filepath = filepath.substring(1);
                    }
                    if (filepath.endsWith("]")) {
                        filepath = filepath.substring(0, filepath.length() - 1);
                    }
                    System.out.println(filepath);
                    File f=new File(filepath);
                    sio=new StructIO(idg);
					map=sio.Read(f);
					setTitle("Simukraft建筑编辑器--for1.7.10-["+f.getName()+"]");
					currentFile=f;
					currentLayer=1;
					author.setText(sio.Author);
					layerSum=map.size();
					Width=map.get(0).getWidth();
					Height=map.get(0).getHeight();
//					ScrollSet(Width,Height);
					height_field.setText(String.valueOf(Height));
					width_field.setText(String.valueOf(Width));
//					System.out.println(Width+" "+Height);
					spinner.setValue(1);
					DrawingPane.setBounds(0,0,Width*16, Height*16);
					Dimension d=DrawingPane.getSize();
//					d.setSize(d.getWidth()+32,d.getHeight()+32);
					int w=(int) (d.getWidth()+32);
					int h=(int) (d.getHeight()+32);
					DrawingPane.setPreferredSize(new Dimension(w,h));
					DrawingPane.updateUI();
                    return true;
				} catch (UnsupportedFlavorException | IOException e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
				return false;
				
			}
			@Override
            public boolean canImport(JComponent comp, DataFlavor[] flavors) {
                for (int i = 0; i < flavors.length; i++) {
                    if (DataFlavor.javaFileListFlavor.equals(flavors[i])) {
                        return true;
                    }
                }
                return false;
            }
		});
		
		Vector<String> blocklist=idg.getList();
		map=new Vector<Map>();
		map.setSize(layerSum);
		for(int i=0;i<layerSum;i++) {
			map.set(i,new Map());
		}
		blocktaking=false;
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JPanel detailpanel = new JPanel();
		detailpanel.setBounds(60, 238, 226, 564);
//		contentPane.add(detailpanel);
		GridBagLayout gbl_detailpanel = new GridBagLayout();
		gbl_detailpanel.columnWidths = new int[]{26, 80, 63, 26, 26, 0};
		gbl_detailpanel.rowHeights = new int[]{21, 23, 0, 0, 0, 0};
		gbl_detailpanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_detailpanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		detailpanel.setLayout(gbl_detailpanel);
		
		JLabel lblNewLabel_1 = new JLabel("\u957F");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		detailpanel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		height_field = new JTextField();
		GridBagConstraints gbc_height_field = new GridBagConstraints();
		gbc_height_field.anchor = GridBagConstraints.NORTHWEST;
		gbc_height_field.insets = new Insets(0, 0, 5, 5);
		gbc_height_field.gridx = 1;
		gbc_height_field.gridy = 2;
		detailpanel.add(height_field, gbc_height_field);
		height_field.setHorizontalAlignment(SwingConstants.CENTER);
		height_field.setText("32");
		height_field.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("\u65B9\u5757");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 2;
		gbc_lblNewLabel_2.gridy = 2;
		detailpanel.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u5BBD");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 3;
		detailpanel.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		width_field = new JTextField();
		GridBagConstraints gbc_width_field = new GridBagConstraints();
		gbc_width_field.anchor = GridBagConstraints.WEST;
		gbc_width_field.insets = new Insets(0, 0, 5, 5);
		gbc_width_field.gridx = 1;
		gbc_width_field.gridy = 3;
		detailpanel.add(width_field, gbc_width_field);
		width_field.setText("32");
		width_field.setHorizontalAlignment(SwingConstants.CENTER);
		width_field.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("\u65B9\u5757");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 2;
		gbc_lblNewLabel_6.gridy = 3;
		detailpanel.add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		JButton btnNewButton_2 = new JButton("\u786E\u5B9A");
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnNewButton_2.gridwidth = 2;
		gbc_btnNewButton_2.gridx = 3;
		gbc_btnNewButton_2.gridy = 4;
		detailpanel.add(btnNewButton_2, gbc_btnNewButton_2);
		
		JSplitPane splitPane_1 = new JSplitPane();
		sl_contentPane.putConstraint(SpringLayout.NORTH, splitPane_1, 5, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, splitPane_1, 5, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, splitPane_1, 6, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, splitPane_1, 5, SpringLayout.EAST, contentPane);
		splitPane_1.setContinuousLayout(true);
		contentPane.add(splitPane_1);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setContinuousLayout(true);
		splitPane.setDividerLocation(200);
		splitPane_1.setRightComponent(splitPane);
		
		list = new JList<String>();
		list.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if(e.getButton()==MouseEvent.BUTTON2)return;
				System.out.println("[Selector Log]Selected eleNo="+list.getSelectedIndex());
				BlockStaus.setText("添加方块 已选中:"+list.getSelectedValue());
				blocktaking=false;
//				taker.setState(false);
				int index=list.getSelectedIndex();
				if(index>=0)img=getImage(index);
			}
		});
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getButton()==MouseEvent.BUTTON2)return;
				System.out.println("[Selector Log]Selected eleNo="+list.getSelectedIndex());
				BlockStaus.setText("添加方块 已选中:"+list.getSelectedValue());
				int index=list.getSelectedIndex();
				if(index>=0)img=getImage(index);
			}
		});
		list.setListData(blocklist);
		list.setSelectedIndex(0);
		System.out.println("[Selector Log]Selected eleNo="+list.getSelectedIndex());
		BlockStaus.setText("添加方块 已选中:"+list.getSelectedValue());
		
		JScrollPane scrollPane = new JScrollPane(list);
		splitPane.setLeftComponent(scrollPane);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		BlockStaus.setFont(new Font("宋体", Font.PLAIN, 10));
		
		BlockStaus.setHorizontalAlignment(SwingConstants.CENTER);
		scrollPane.setColumnHeaderView(BlockStaus);
		
		JToolBar toolBar = new JToolBar();
		scrollPane.setRowHeaderView(toolBar);
		toolBar.setOrientation(SwingConstants.VERTICAL);
		
		taker = new JToggleButton("");
		taker.setToolTipText("\u56FE\u5757\u9009\u62E9\u5668(I)");
		taker.setIcon(new ImageIcon(Main.class.getResource("/com/qmasoft/images/taker.png")));
		taker.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				taker.requestFocus();
				if(!taker.isSelected())taker.setSelected(true);
				else taker.setSelected(false);
				blocktaking=taker.isSelected();
			}}
		,KeyStroke.getKeyStroke(KeyEvent.VK_I,ActionEvent.CTRL_MASK),JComponent.WHEN_IN_FOCUSED_WINDOW);
		
		taker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				blocktaking=taker.isSelected();
			}
		});
		toolBar.add(taker);
		DrawingPane=new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -3357518274247948930L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				fresh(g);
			}
		};
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		DrawingPane.setPreferredSize(new Dimension(32*16+32,32*16+32));
		splitPane.setRightComponent(scrollPane_1);
		
		scrollPane_1.setViewportView(DrawingPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		splitPane_1.setLeftComponent(tabbedPane);
		
//		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("文档属性", null, detailpanel, null);
		//		DrawingPane.setBackground(new Color(187, 255, 187));
		DrawingPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				//AddBlock
				int x=e.getX();
				int y=e.getY();
				x=x-(x%16);
				y=y-(y%16);
				if(x/16<Width&&y/16<Height&&x>=0&&y>=0) {
					if(blocktaking) {
						list.setSelectedIndex(map.get(currentLayer-1).getPoint(x/16, y/16));
						BlockStaus.setText("添加方块 已选中:"+list.getSelectedValue());
						int index=list.getSelectedIndex();
						if(index>=0)img=getImage(index);
					}else {
						map.get(currentLayer-1).setPoint(x/16, y/16, list.getSelectedIndex());
						DrawingPane.getGraphics().drawImage(img, x, y, 16, 16, null);
						if(grid) {
							Graphics2D g2=(Graphics2D)DrawingPane.getGraphics();
							final float pt[]={5,5};
							BasicStroke b=new BasicStroke(1,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,0,pt,0);
							g2.setStroke(b);
							g2.drawLine(x,y,x+16,y);
							g2.drawLine(x,y+16,x+16,y+16);
							g2.drawLine(x,y,x,y+16);
							g2.drawLine(x+16,y,x+16,y+16);
						}
					}
					
				}
//				for(int i=0;i<32;i++) {
//					for(int j=0;j<32;j++) {
//						System.out.print(map.getPoint(j, i));
//					}
//					System.out.println();
//				}
			}
		});
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Width=Integer.valueOf(width_field.getText());
				Height=Integer.valueOf(height_field.getText());
//				ScrollSet(Width,Height);
				DrawingPane.setSize(Width*16, Height*16);
				for(int i=0;i<map.size();i++) {
					map.get(i).setHeight(Height);
//					System.out.print("height set ok");
					map.get(i).setWidth(Width);
				}
				Dimension d=DrawingPane.getSize();
//				d.setSize(d.getWidth()+32,d.getHeight()+32);
				int w=(int) (d.getWidth()+32);
				int h=(int) (d.getHeight()+32);
				DrawingPane.setPreferredSize(new Dimension(w,h));
			}
		});
		
		setSize(944,638);
//		addComponentListener(new ComponentAdapter() {
//			@Override
//			public void componentResized(ComponentEvent e) {
//				drawpane.setBounds(0, 0, getWidth(), getHeight());
//			}
//		});
	}
	public void openurl(String url) throws URISyntaxException, IOException {
		Desktop dsk=Desktop.getDesktop();
		if(dsk.isSupported(Desktop.Action.BROWSE)&&Desktop.isDesktopSupported()) {
			URI uri=new URI(url);
			dsk.browse(uri);
		}
	}
//	public void ScrollSet(int Width,int Height) {
//		scrollBarH.setValue(0);
//		scrollBarH.setMaximum(Height*16);
//		scrollBarW.setValue(0);
//		scrollBarW.setMaximum(Width*16);
//		scrollBarH.setUnitIncrement(Height/100);
//		scrollBarW.setUnitIncrement(Width/100);
//		scrollBarW.setBlockIncrement(Width/100);
//		scrollBarH.setBlockIncrement(Height/100);
//	}
	public Image getImage(int index) {
		selectedID=idg.getIDString(index);
		selectedID=selectedID.replace(":","-");
		Image r=null;
		try {
			r=Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/com/qmasoft/images/textures/"+selectedID+".png"));
		}catch(NullPointerException e) {
			return null;
		}
		return r;
	}
	public void clearCanvas(int layer) {
		for(int i=0;i<Height;i++) {
			for(int j=0;j<Width;j++) {
				map.get(layer).setPoint(j,i,0);
			}
		}
		fresh();
	}
	public void save(boolean saveas) {
		if(author.getText().equals("")) {
			JOptionPane.showMessageDialog(contentPane, "请填写作者！", getTitle(), JOptionPane.ERROR_MESSAGE, null);
			return;
		}
		if(saveas) {
			JFileChooser jf=new JFileChooser();
			int code=jf.showSaveDialog(contentPane);
			if(code==JFileChooser.CANCEL_OPTION)return;
			currentFile=jf.getSelectedFile();
			setTitle("Simukraft建筑编辑器-["+jf.getSelectedFile().getName()+"]");
		}
		sio.Write(currentFile, author.getText(), Width, Height, layerSum, map, idg);
	}
	public void fresh() {
		fresh(DrawingPane.getGraphics());
		DrawingPane.updateUI();
	}
	public void fresh(Graphics g) {
		int index=currentLayer-1;
		Map m=map.get(index);
		Image pen=null;
		for(int i=0;i<m.getHeight();i++) {
			for(int j=0;j<m.getWidth();j++) {
				pen=getImage(m.getPoint(j, i));
				if(pen==null) {
					System.err.println("[Error]Cannot find image "+idg.getIDString(m.getPoint(j,i)));
				}
				g.drawImage(pen, j*16, i*16, 16, 16, null);
			}
		}
		if(grid) {
			showGrid(g,m.getWidth(),m.getHeight());
		}
	}
	public void showGrid(Graphics g,int w,int h) {
		Graphics2D g2=(Graphics2D)g;
		final float pt[]={5,5};
		BasicStroke b=new BasicStroke(1,BasicStroke.CAP_BUTT,BasicStroke.JOIN_BEVEL,0,pt,0);
		g2.setStroke(b);
		for(int i=1;i<h;i++) {
			g2.drawLine(0, i<<4, w<<4, i<<4);
		}
		for(int j=1;j<w;j++) {
			g2.drawLine(j<<4, 0, j<<4, h<<4);
		}
	}
}
